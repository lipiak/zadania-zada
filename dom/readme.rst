Praca domowa #A
===============

Korzystając z dowolnego frameworka webowego stworzyć projekt korzystający
z rozproszonych zadań (conajmniej dwojakiego rodzaju) realizowanych w tle z kilkoma workerami.
Interfejs ma być przyjazny dla użytkownika i w żadnym wypadku się nie zacinać.
Dla długotrwałych zadań (np. pakowanie lub rozpakowywanie pliku zip) należy wyświetlić użytkownikowi odpowiednią informację oraz pasek postępu.


Przykładowy projekt
-------------------

Galeria zdjęć dla wielu uzytkowników posiadająca następujące funkcjonalności:

* Dodawanie pojedynczego zdjęcia
* Dodawanie pliku zip z wieloma zdjęciami, które aplikacja rozpakowuje i wrzuca do galerii
* Tworzenia katalogów i przenoszenie zdjęć między nimi
* Wyświetlanie zawartości katalogu w postaci miniatur o różnych wielkościach (co najmniej 3 do dyspozycji).
  Miniatury powinny być przeskalowanymi obrazkami po stronie serwera a nie pełnowymiarowymi tylko przeskalowanymi
  po stronie przeglądarki za pomocą styli, atrybutów html czy javascriptu.
* Zapisywanie pojedynczego zdjęcia na dysk.
* Zapisywanie wszystkich zdjęć z wybranego katalogu w postaci pliku zip.

